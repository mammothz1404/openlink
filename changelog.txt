v1.5.4
- fixed export of signed APK ("Could not find com.android.tools.lint:lint-gradle:26.1.2.")

v1.5.3
- removed flicker in info screen for old Android versions (Gingerbread, ...)
- minor code improvements

v1.5.2
- added Google repo for Gradle plugin in order for F-Droid to be able to build the project

v1.5.1
- tried to fix mysterious runtime exception

v1.5.0
- added possibility to open links in incognito tab if supported browser is installed (Thanks to Ildar Mulyukov for the idea.)
- Wikipedia links which end with ")" are opened correctly now
- improved Russian translation (Thanks to Ildar Mulyukov for the PR.)
- set max. API level to 27

v1.4.6
- improved typography (Thanks to Nave for the PR.)

v1.4.5
- added "<" and ">" to the characters to be ignored (Thanks to SN for the bug report.)

v1.4.4.x
- downgraded gradle to last stable version
- try to persuade F-Droid to build new release

v1.4.4
- Added checks to prevent crashes if device can't open links to video or source code
- updated gradle to 3.0.0-alpha3

v1.4.3
- added Hindi translation
- set max. API level to 25 (Android 7.1)
- updated gradle to 2.2.3

v1.4.2
- set max. API level to 24 (Android 7)
- minor code changes due to new max API level
- updated gradle to 2.1.3

v1.4.1
- set minimum API level to 3 (Android 1.5)
- minor code improvements
- tried to improve Chinese translations (added traditional and simplified) in addition to regular zn-locale

v1.4.0
- improved icons
- removed entry from launcher, Activity can only be accessed via "open"-button in "Play Store"- oder "F-Droid"-app
- changed max. API level to 23 (Android 6)

v1.3.1
- updated source code repository URL (moved from Gitorious to GitLab)
- improved layout
- changed max. API level to 22 (Android 5.1)

v1.3
- support for Material design (Android 5.0 and above)
- improved URL parsing (thanks to R. Wong)

v1.2.1
- removed flicker when links are opened

v1.2
- added translations for the following languages: Arabic, Bulgarian, Catalan, Czech, Danish, Spanish, Finnish, French, Italian, Japanese, Dutch, Norwegian, Polish, Portuguese, Russian, Swedish, Thai, Chinese

v1.1
- brackets and punctuation characters enclosing URLs will be ignored (Thanks to DB for the idea.)

v1.0.2
- more UI improvements, friendlier info screen

v1.0.1
- minor UI improvements in info screen

v1.0
- initial release


